import { defineStore } from 'pinia';
import ProductModel from "@/models/ProductModel";

// Fake API
const BASE_URL = 'https://fakestoreapi.com';

// Define the products store
export const productsStore = defineStore('products', {
    state: () => ({
        products: [],
        product: { ...ProductModel },
        cart: [],
        payment: {},
        categories:[],
        productToSell: {}
    }),

    actions: {
        // Method to fetch products from the fake API
        async fetchProductsFromDB(category = '') {
            let url = `${BASE_URL}/products`;
            if (category) {
                url += `/category/${category}`;
            }

            try {
                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error('Failed to fetch products');
                }
                this.products = await response.json();
            } catch (error) {
                console.error(error);
            }
        },

        // Method to fetch a  product by Id from the API
        async getProductById(id) {
            try {
                const response = await fetch(`${BASE_URL}/products/${id}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch product');
                }
                this.product = await response.json();
            } catch (error) {
                console.error(error);
            }
        },
        // Method to fetch categories from the API
        async fetchCategories() {
            try {
                const response = await fetch(`${BASE_URL}/products/categories`);
                if (!response.ok) {
                    throw new Error('Failed to fetch categories');
                }
                this.categories = await response.json();
            } catch (error) {
                console.error(error);
            }
        },
        // Method to add a product to the cart
        addToCart(product) {
            this.cart.push(product);
        },
        // Method to remove a product from the cart
        removeFromCart(id) {
            this.cart = this.cart.filter((item) => item.id !== id);
        },
        // Method to process payment
        processPay(processPaymentInfos) {
            this.productToSell = {
                totalAmount: processPaymentInfos.totalAmount,
                items: processPaymentInfos.items
            };
        }
    }
});
