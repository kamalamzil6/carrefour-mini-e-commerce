import { createRouter, createWebHistory } from 'vue-router'
import ProductDetail from "@/views/ProductDetail.vue";
import Store from "@/views/Store.vue";
import Cart from "@/views/Cart.vue";
import Shipping from "@/views/Shipping.vue";


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'StoreView',
            component: Store
        },
        {
            path: '/product/:id',
            name: 'ProductView',
            component: ProductDetail
        },
        {
            path: '/cart',
            name: 'CartView',
            component: Cart
        },
        {
            path: '/Shipping',
            name: 'ShippingView',
            component: Shipping
        }
    ]
})

export default router
