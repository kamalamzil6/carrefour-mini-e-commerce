export default class AddressModel {
    constructor(street = '', city = '', zip = '') {
        this.street = street;
        this.city = city;
        this.zip = zip;
    }

    isValid() {
        return this.street && this.city && this.zip;
    }
}