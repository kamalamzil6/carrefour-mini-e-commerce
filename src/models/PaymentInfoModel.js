export default class PaymentInfoModel {
    constructor(cardHolder = '', cardNumber = '', cvv = '', expiry = '') {
        this.cardHolder = cardHolder;
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.expiry = expiry;
    }

    isValid() {
        return (
            this.cardHolder &&
            this.cardNumber &&
            this.cvv &&
            this.expiry
        );
    }
}
