<h1> Carrefour - Mini E-commerce Product Gallery Frontend Kata</h1>
<b>Candidat: Kamal AMZIL</b>

Ce document fournit des instructions sur la manière de lancer le projet et l'URL pour tester celui-ci. J'ai répondu à toutes les questions.

Pour réaliser ce projet, j'ai utilisé le framework Vue.js comme vous me l'avez demandé.

J'ai respecté le principe MVP pour livrer ce projet, c'est pourquoi j'ai choisi la bibliothèque Pinia pour la gestion de l'état plutôt que Vuex, en raison des contraintes de temps.

L'Appli est déployée sur GitLab. Vous pouvez la tester en le lien suivant  : [URL du projet](https://carrefour-mini-e-commerce-kamalamzil6-13fa2d794dc4e90ccfa0a3d4f.gitlab.io/)

Comment lancer le projet
    
    > Cloner le projet sur votre machine locale
    > Installer les dépendances : npm install
    > Démarrer le projet : npm run dev


<h2>User Stories et Tâches</h2>

<table>
  <tr>
    <th>User Story</th>
    <th>Tâche</th>
    <th>Statut</th>
  </tr>
  <tr>
    <td rowspan="2">User Story 1</td>
    <td>Fetch and display products from API</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Add "Add to Cart" button</td>
    <td>Done</td>
  </tr>
  <tr>
    <td rowspan="2">User Story 2</td>
    <td>Implement shopping cart</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Allow adjusting quantities</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Advanced Product Details</td>
    <td>Enable detailed product view</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Checkout Process</td>
    <td>Add checkout form</td>
    <td>Done</td>
  </tr>
  <tr>
    <td rowspan="2">Product Search and Filtering</td>
    <td>Implement search by name</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Implement filtering by category or price</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Responsive Layout and Design</td>
    <td>Create responsive design</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>Deployment</td>
    <td>Deploy to GitLab Pages </td>
    <td>Done</td>
  </tr>
</table>

